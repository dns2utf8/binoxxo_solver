use std::fmt::Display;
use std::ops::{Index, IndexMut};
use std::str::FromStr;

mod solver;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Field {
    pub width: usize,
    data: Vec<FieldState>,
}

impl FromStr for Field {
    type Err = FieldErrors;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut data = Vec::with_capacity(input.len() / 2 + 1);
        let mut width = None;

        for (line_no, line) in input.trim_start_matches('\n').lines().enumerate() {
            let mut line_width = 0;
            for (i, c) in line.chars().enumerate() {
                let f = match c {
                    ' ' => Unknown,
                    'X' => X,
                    'O' => O,

                    '|' => {
                        /* separator */
                        continue;
                    }
                    _ => {
                        return Err(FieldErrors::InvalidSymbol(Span {
                            line: line.to_string(),
                            line_no,
                            char_start: i,
                            char_end: i + 1,
                        }));
                    }
                };

                line_width += 1;
                data.push(f);
            }

            match width {
                None => width = Some(line_width),
                Some(width) if width == line_width => { /* all good */ }
                Some(width) => {
                    return Err(FieldErrors::InputNotSquare {
                        expected_width: width,
                        current_width: line_width,
                        span: Span {
                            line: line.to_string(),
                            line_no,
                            char_start: 0,
                            char_end: line.len(),
                        },
                    })
                }
            }
        }

        let width = match width {
            Some(width) => {
                let square = width * width;
                if square == data.len() {
                    width
                } else if data.len() < square {
                    println!("{:?}", data);
                    return Err(FieldErrors::IncompleteInput);
                } else {
                    println!("{:?}", data);
                    return Err(FieldErrors::TooMuchInput);
                }
            }
            None => return Err(FieldErrors::InvalidInput),
        };

        Ok(Field { width, data })
    }
}

impl Field {
    pub fn new_random(width: usize) -> Field {
        use rand::prelude::*;
        let mut rng = rand::thread_rng();
        let data = (0..width * width)
            .map(|_| {
                if rng.gen() {
                    Unknown
                } else {
                    if rng.gen() {
                        X
                    } else {
                        O
                    }
                }
            })
            .collect();
        Field { width, data }
    }

    pub fn seal(&mut self) {
        for e in &mut self.data {
            *e = match &*e {
                X => Xs,
                O => Os,
                others => (others).clone(),
            };
        }
    }

    pub fn is_complete(&self) -> bool {
        self.data.iter().any(|f| *f == Unknown) == false
    }
    pub fn is_valid(&self) -> bool {
        (0..self.width)
            .map(|row| self.is_row_valid(row))
            .chain((0..self.width).map(|col| self.is_col_valid(col)))
            .any(|pred| pred == false)
            == false
    }
    pub fn is_row_valid(&self, row: usize) -> bool {
        let row = self.row(row);
        //println!("{:?} = {}", row, validate(&row));
        validate(&row)
    }
    pub fn is_col_valid(&self, col: usize) -> bool {
        let col = self.col(col);
        //println!("{:?} = {}", col, validate(&col));
        validate(&col)
    }
    fn row(&self, row: usize) -> Vec<FieldState> {
        let start = row * self.width;
        let end = (row + 1) * self.width;
        self.data[start..end].iter().cloned().collect()
    }
    fn col(&self, col: usize) -> Vec<FieldState> {
        (0..self.width)
            .map(|i| self.width * i + col)
            .map(|i| self.data[i].clone())
            .collect()
    }
    pub fn solve(&mut self) -> Result<Field, solver::SolveError> {
        if self.is_valid() == false {
            return Err(solver::SolveError::InvalidStartState);
        }
        let start = {
            let mut f = self.clone();
            f.seal();
            f
        };

        solver::solve(start)
    }
    pub fn clear(&mut self) {
        for e in &mut self.data {
            *e = Unknown;
        }
    }
}

impl Display for Field {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        //write!(f, "({}, {})", self.x, self.y)
        let valid_cols = (0..self.width)
            .map(|i| self.is_col_valid(i))
            .map(|b| if b { "T " } else { "f " })
            .collect::<String>();

        let table = (0..self.width)
            .map(|i| {
                let row = self.row(i);
                let rows = row
                    .iter()
                    .map(|s| match s {
                        Unknown => " ",
                        X => "x",
                        Xs => "X",
                        O => "o",
                        Os => "O",
                    })
                    .collect::<Vec<_>>()
                    .join("|");
                format!("{} {}\n", rows, if validate(&row) { "T" } else { "f" })
            })
            .collect::<String>();

        write!(f, "{}\n{}{}\n", valid_cols, table, valid_cols)
    }
}

impl Index<[usize; 2]> for Field {
    type Output = FieldState;

    fn index(&self, pos: [usize; 2]) -> &Self::Output {
        let idx = self.width * pos[1] + pos[0];
        &self.data[idx]
    }
}

impl IndexMut<[usize; 2]> for Field {
    fn index_mut(&mut self, pos: [usize; 2]) -> &mut Self::Output {
        let idx = self.width * pos[1] + pos[0];
        &mut self.data[idx]
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct Span {
    //symbol: char,
    line: String,
    line_no: usize,
    char_start: usize,
    char_end: usize,
}

#[derive(Debug, PartialEq, Eq)]
pub enum FieldErrors {
    InputNotSquare {
        expected_width: usize,
        current_width: usize,
        span: Span,
    },
    InvalidSymbol(Span),
    IncompleteInput,
    InvalidInput,
    TooMuchInput,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum FieldState {
    /// Blank
    Unknown,
    /// Editable
    X,
    /// Sealed variant
    Xs,
    /// Editable
    O,
    /// Sealed variant
    Os,
}
use FieldState::*;

impl FieldState {
    pub fn is_sealed(&self) -> bool {
        match self {
            Unknown | X | O => false,
            Xs | Os => true,
        }
    }
}

/// TODO better errors
impl FromStr for FieldState {
    type Err = &'static str;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        if input.len() != 1 {
            return Err("wrong length");
        }
        Ok(match input.chars().next().unwrap() {
            ' ' => Unknown,
            'X' | 'x' => X,
            'O' | 'o' => O,
            _ => return Err("invalid_input_symbol"),
        })
    }
}

fn validate(sequence: &Vec<FieldState>) -> bool {
    let mut last = None;
    let mut last2 = None;
    let (mut os, mut xs) = (0, 0);
    let max = ((sequence.len() as f64) / 2.0).ceil() as usize;

    for symbol in sequence.iter() {
        match symbol {
            X | Xs => xs += 1,
            O | Os => os += 1,
            Unknown => { /* noop */ }
        }
        match last {
            None => last = Some(symbol),
            Some(l) => {
                match last2 {
                    None => {}
                    Some(l2) => {
                        match symbol {
                            Unknown => { /* ok */ }
                            X | Xs | O | Os => {
                                if symbol == l && symbol == l2 {
                                    return false;
                                }
                            }
                        }
                    }
                }
                last2 = Some(l);
                last = Some(symbol);
            }
        }
    }
    if xs > max || os > max {
        false
    } else {
        true
    }
}

#[cfg(test)]
mod display {
    use super::*;

    #[test]
    fn f3() {
        let f = Field::from_str(
            "
X|O|X
O| |O
X|O|X
",
        )
        .unwrap();
        let e = "T T T \nx|o|x T\no| |o T\nx|o|x T\nT T T \n";
        let d = format!("{}", f);

        assert_eq!(e, d);
    }

    #[test]
    fn f3_sealed() {
        let mut f = Field::from_str(
            "
X|O|X
O| |O
X|O|X
",
        )
        .unwrap();

        f.seal();

        let e = "T T T \nX|O|X T\nO| |O T\nX|O|X T\nT T T \n";
        let d = format!("{}", f);

        assert_eq!(e, d);
    }

    #[test]
    fn f3_invalid() {
        let f = Field::from_str(
            "
X|X|X
O| |O
X|O|X
",
        )
        .unwrap();
        let e = "T T T \nx|x|x f\no| |o T\nx|o|x T\nT T T \n";
        let d = format!("{}", f);

        assert_eq!(e, d);
    }

    #[test]
    fn f3_invalid_sealed() {
        let mut f = Field::from_str(
            "
X| |X
O| |O
X|O|X
",
        )
        .unwrap();

        f.seal();

        f[[1, 0]] = X;

        let e = "T T T \nX|x|X f\nO| |O T\nX|O|X T\nT T T \n";
        let d = format!("{}", f);

        assert_eq!(e, d);
    }
}

#[cfg(test)]
mod field_state {
    use super::*;

    #[test]
    fn incomplete() {
        assert!(X.is_sealed() == false);
        assert!(O.is_sealed() == false);
        assert!(Unknown.is_sealed() == false);
        assert!(Xs.is_sealed());
        assert!(Os.is_sealed());
    }
}

#[cfg(test)]
mod validator {
    use super::*;

    #[test]
    fn validate_0() {
        assert!(validate(&vec![]));
    }
    #[test]
    fn validate_1() {
        assert!(validate(&vec![X]));
    }
    #[test]
    fn validate_2() {
        assert!(validate(&vec![X, O]));
    }
    #[test]
    fn validate_3() {
        assert!(validate(&vec![X, X, O]));
    }
    #[test]
    fn validate_4() {
        assert!(validate(&vec![X, X, O, O]));
    }

    #[test]
    fn validate_3_f() {
        assert!(validate(&vec![X, X, X]) == false);
    }
    #[test]
    fn validate_4_f() {
        assert!(validate(&vec![O, X, X, X]) == false);
    }
    #[test]
    fn validate_6() {
        assert!(validate(&vec![O, X, O, O, X, X]));
    }
    #[test]
    fn validate_6_f() {
        assert!(validate(&vec![O, X, X, O, X, X]) == false);
    }

    #[test]
    fn two_by_two_valid() {
        let f = Field::from_str("\nX|O\nO|X\n").unwrap();
        assert!(f.is_complete());
        assert!(f.is_valid());
    }

    #[test]
    fn three_by_tree_partial_invalid() {
        let f = Field::from_str(
            "
X|X|X
O| |O
X|O|X
",
        )
        .unwrap();
        println!("{}", f);
        assert!(f.is_complete() == false);

        assert!(f.is_col_valid(0), "col(0)");
        assert!(f.is_col_valid(1), "col(1)");
        assert!(f.is_col_valid(2), "col(2)");

        assert!(f.is_row_valid(0) == false, "row(0)");
        assert!(f.is_row_valid(1), "row(1)");
        assert!(f.is_row_valid(2), "row(2)");

        assert!(f.is_valid() == false);
    }

    #[test]
    fn three_by_tree_invalid() {
        let f = Field::from_str("X|X|X\nX|X|X\nX|X|X\n").unwrap();
        println!("{:?}", f);
        assert!(f.is_complete());

        assert!(f.is_col_valid(0) == false, "col(0)");
        assert!(f.is_col_valid(1) == false, "col(1)");
        assert!(f.is_col_valid(2) == false, "col(2)");

        assert!(f.is_row_valid(0) == false, "row(0)");
        assert!(f.is_row_valid(1) == false, "row(1)");
        assert!(f.is_row_valid(2) == false, "row(2)");

        assert!(f.is_valid() == false);
    }
}

#[cfg(test)]
mod parser {
    use super::*;
    use FieldErrors::*;

    #[test]
    fn invalid_input() {
        let e = Field::from_str("");
        assert_eq!(Err(InvalidInput), e);
    }
    #[test]
    fn invalid_non_square_input() {
        let e = Field::from_str("X|O\nO|X|O\n");
        let span = Span {
            line: "O|X|O".to_string(),
            line_no: 1,
            char_start: 0,
            char_end: 5,
        };
        assert_eq!(
            Err(InputNotSquare {
                expected_width: 2,
                current_width: 3,
                span
            }),
            e
        );
    }
    #[test]
    fn invalid_input_symbol() {
        let e = Field::from_str("X| \nX| \nX| \nF| \n");
        let span = Span {
            line: "F| ".to_string(),
            line_no: 3,
            char_start: 0,
            char_end: 1,
        };
        assert_eq!(Err(InvalidSymbol(span)), e);
    }
    #[test]
    fn three_by_one_invalid() {
        let f = Field::from_str("X|X|X\n");
        assert_eq!(Err(IncompleteInput), f);
    }

    #[test]
    fn two_by_two_valid() {
        let f = Field::from_str("X|O\nO|X\n");
        let exp = Field {
            data: vec![X, O, O, X],
            width: 2,
        };
        assert_eq!(Ok(exp), f);
    }

    #[test]
    fn two_by_two_empty() {
        let f = Field::from_str("X|O\nO| \n");
        let exp = Field {
            data: vec![X, O, O, Unknown],
            width: 2,
        };
        assert_eq!(Ok(exp), f);
    }

    #[test]
    fn clear() {
        let mut f = Field::from_str("X|O\nO|X\n").unwrap();
        f.clear();
        let exp = Field {
            data: vec![Unknown, Unknown, Unknown, Unknown],
            width: 2,
        };
        assert_eq!(exp, f);
    }
}
