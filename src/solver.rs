use crate::{Field, FieldState};

use FieldState::*;

#[derive(Debug, PartialEq, Eq)]
pub enum SolveError {
    /// happens when one tries to solve a Field that is already unsolvable
    InvalidStartState,
    Unsolvable,
    ProblemTooBigForBruteForce,
}

pub fn solve(start: Field) -> Result<Field, SolveError> {
    let n_unknowns = start.data.iter().filter(|f| **f == Unknown).count() as u32;
    let mut field = start.clone();

    if n_unknowns > 64 {
        return Err(SolveError::ProblemTooBigForBruteForce);
    }

    for state in 0usize..2usize.pow(n_unknowns) {
        fill_field(&mut field, state);
        if field.is_valid() {
            return Ok(field);
        }
    }
    Err(SolveError::Unsolvable)
}

fn fill_field(f: &mut Field, state: usize) {
    let mut mask = 1usize;
    for e in &mut f.data {
        match e {
            X | O | Unknown => {
                let s = state & mask;
                mask <<= 1;

                *e = if s > 0 { X } else { O };
            }
            Xs | Os => { /* ignore */ }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::str::FromStr;

    #[test]
    fn incomplete() {
        let mut f = Field::from_str("X|O\n |X\n").unwrap();
        assert!(f.is_complete() == false);
        assert!(f.is_valid());
        f.seal();

        fill_field(&mut f, 0);
        let mut filled = Field::from_str("X|O\nO|X\n").unwrap();
        filled.seal();
        filled[[0, 1]] = O;
        assert_eq!(filled, f);
        assert!(f.is_valid());
    }

    #[test]
    fn mut_incomplete() {
        let mut f = Field::from_str("\nX|O\n |X\n").unwrap();
        assert!(f.is_complete() == false);
        assert!(f.is_valid());

        f[[0, 1]] = O;
        assert!(f.is_complete());
        assert!(f.is_valid());
    }

    #[test]
    fn mut_incomplete_3() {
        let mut f = Field::from_str(
            "
X|O|X
O| |O
X|O|X
",
        )
        .unwrap();
        assert!(f.is_complete() == false);
        assert!(f.is_valid());

        f[[1, 1]] = O;
        assert!(f.is_complete());
        assert!(f.is_valid() == false);

        f[[1, 1]] = X;
        assert!(f.is_complete());
        assert!(f.is_valid());
    }
    #[test]
    fn solve_simple() {
        let mut f = Field::from_str("X|O\n |X\n").unwrap();
        assert!(f.is_complete() == false);
        assert!(f.is_valid());
        f.seal();

        let mut solved = Field::from_str("X|O\nO|X\n").unwrap();
        solved.seal();
        solved[[0, 1]] = O;
        assert_eq!(Ok(solved), solve(f));
    }
}
