use std::str::FromStr;

use binoxxo_solver::{Field, FieldErrors};

fn main() -> Result<(), FieldErrors> {
    let mut f = Field::from_str(
        r#"
X| |X
O| |O
X| |X
"#,
    )?;

    f.seal();

    while f.is_complete() == false || f.is_valid() == false {
        print!("\n\n{}\nChange: (x, y, X|O) > ", f);
        flush();
        let mut input = String::new();
        match std::io::stdin().read_line(&mut input) {
            Ok(_n) => {
                //println!("{} bytes read", _n);
                //println!("{}", input);

                if input.trim() == "s" {
                    println!("Solving ...");
                    flush();
                    match f.solve() {
                        Ok(field) => {
                            println!("Hurray, we found a solution:");
                            f = field;
                        }
                        Err(error) => println!("No luck: {:?}", error),
                    }
                } else {
                    let mut parts = input.splitn(3, ",").map(|p| p.trim())//.collect::<Vec<_>>()
                    ;
                    let x = parts.next().unwrap().parse().unwrap();
                    let y = parts.next().unwrap().parse().unwrap();
                    let symbol = parts.next().unwrap().parse().unwrap();

                    if f[[x, y]].is_sealed() == false {
                        f[[x, y]] = symbol;
                    } else {
                        println!("field is sealed!");
                    }
                }
            }
            Err(error) => println!("error: {}", error),
        }
    }

    println!("{}\nsolved!", f);

    Ok(())
}

fn flush() {
    use std::io::{self, Write};
    io::stdout().flush().expect("unable to flush stdout");
}
